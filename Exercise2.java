public class Exercise2 {
    public static void main(String[] args) {
        // a):
        int a = 0;
        // b):
        String b = "hi";
        // d):
        Value d = new Value();
    }
}

// c):
class Value {
    private int val;
}