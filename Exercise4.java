import java.util.Arrays;

public class Exercise4 {
    public static int sum(int[] a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }
    public static int[] cumsum(int[] a) {
        int[] cumsum = new int[a.length];
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
            cumsum[i] = sum;
        }
        return cumsum;
    }
    public static int[] positives(int[] a) {
        int num_of_positives = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] > 0) {
                num_of_positives += 1;
            }
        }
        int[] positives = new int[num_of_positives];
        int j = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] > 0) {
                positives[j++] = a[i];
            }
        }
        return positives;
    }
    public static void main(String[] args) {
        System.out.println(sum(new int[] {1, 2, 3}));
        System.out.println(Arrays.toString(cumsum(new int[] {1, 2, 3})));
        System.out.println(Arrays.toString(positives(new int[] {1, -1, -2, 4, 0, 5, -20})));
    }
}
