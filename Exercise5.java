import java.util.Arrays;

public class Exercise5 {
    public static float[][] createSquareMatrix(int n) {
        // Initialised to 0 by default
        return new float[n][n];
    }
    public static float[][] createSquareMatrix(int n, float value) {
        float[][] matrix = new float[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = value;
            }
        }
        return matrix;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(createSquareMatrix(5)));
        System.out.println(Arrays.deepToString(createSquareMatrix(3, 3.14f)));
    }
}
