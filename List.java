class ListElement {
    private ListElement next;
    private int value;

    public ListElement(int value, ListElement next) {
        this.value = value;
        this.next = next;
    }
    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }

    public ListElement getNext() {
        return next;
    }

    public void setNext(ListElement next) {
        this.next = next;
    }
}

public class List {
    private ListElement first;
    public List() {}
    private ListElement getElement(int position) {
        ListElement current = first;
        for (int i = 0; i < position; i++) {
            if (current == null) {
                return null;
            }
            current = current.getNext();
        }
        return current;
    }
    public boolean add(int value, int position) {
        if (position == 0) {
            if (first == null) {
                first = new ListElement(value, null);
                return true;
            }
            first.setNext(new ListElement(value, first.getNext()));
            return true;
        }
        ListElement element = getElement(position - 1);
        if (element == null) {
            return false;
        }
        element.setNext(new ListElement(value, element.getNext()));
        return true;
    }
    public boolean remove(int position) {
        if (position == 0) {
            first = first.getNext();
            return true;
        }
        ListElement element = getElement(position - 1);
        if (element == null | element.getNext() == null) {
            return false;
        }
        element.setNext(element.getNext().getNext());
        return true;
    }
    public Integer get(int position) {
        ListElement element = getElement(position);
        if (element == null) {
            return null;
        }
        return element.getValue();
    }
    public int length() {
        ListElement current = first;
        int length;
        for (length = 0; current != null; length++) {
            current = current.getNext();
        }
        return length;
    }

    public static List createCycle() {
        ListElement a = new ListElement(0, null);
        ListElement b = new ListElement(1, null);
        ListElement c = new ListElement(2, null);
        a.setNext(b);
        b.setNext(c);
        c.setNext(b);
        List listWithCycle = new List();
        listWithCycle.first = a;
        return listWithCycle;
    }

    public boolean hasCycle() {
        if (first == null) {
            return false;
        }
        ListElement current1 = first;
        ListElement current2 = first;
        do {
            current1 = current1.getNext();
            current2 = current2.getNext();
            if (current2 == null) {
                return false;
            }
            current2 = current2.getNext();
        } while (current1 != current2 && current2 != null);
        return current1 == current2 && current1 != null;
    }

    public static void main(String[] args) {
        List myList = new List();
        System.out.println(myList.add(2, 2));
        System.out.println(myList.add(0, 0));
        System.out.println(myList.add(1, 1));
        System.out.println(myList.add(2, 2));
        System.out.println(myList.length());
        System.out.println(myList.remove(3));
        System.out.println(myList.remove(1));
        System.out.println(myList.get(1));
        System.out.println(myList.length());
        List myCircularList = List.createCycle();
        System.out.println(myCircularList.hasCycle());
        System.out.println(myList.hasCycle());
    }
}