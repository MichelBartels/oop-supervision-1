class StackElement {
    StackElement next;
    int value;
    public StackElement(StackElement next, int value) {
        this.next = next;
        this.value = value;
    }
}
public class Stack {
    StackElement head;
    public Stack() {}
    public void push(int value) {
        head = new StackElement(head, value);
    }
    public Integer head() {
        if (head == null) {
            return null;
        }
        return head.value;
    }
    public Integer pop() {
        if (head == null) {
            return null;
        }
        int value = head.value;
        head = head.next;
        return value;
    }

    public static void main(String[] args) {
        Stack myStack = new Stack();
        myStack.push(0);
        myStack.push(1);
        myStack.push(2);
        System.out.println(myStack.pop());
        myStack.push(3);
        System.out.println(myStack.pop());
        System.out.println(myStack.head());
        myStack.pop();
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
    }
}
